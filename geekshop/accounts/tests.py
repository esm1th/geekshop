from django.test import TestCase
from django.test.client import Client
from django.core.management import call_command
from django.conf import settings

from .models import User
from basketapp.models import Basket

# Create your tests here.


class TestUserAuth(TestCase):
    """Testing authenticating users"""

    def setUp(self):

        call_command('flush', '--noinput')
        call_command('loaddata', 'test_db.json')

        self.client = Client()
        self.superuser = User.objects.create_superuser(
            'super_django_user', 'super_jango@gmail.com', 'password')
        self.user = User.objects.create_user(
            'simple_user', 'simple_user@gmail.com', 'password')
        self.user_with_first_name = User.objects.create_user(
            'named_simple_user', 'named_user@gmail.com', 'password', first_name='First')

    def test_user_login(self):

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_anonymous)

        self.client.login(username='simple_user', password='password')

        response = self.client.get('/products/')
        self.assertFalse(response.context['user'].is_anonymous)
        self.assertEqual(response.context['user'], self.user)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['user'], self.user)

    def test_basket_login_redirect(self):

        response = self.client.get('/basket/items/')
        self.assertEqual(response.url, '/accounts/login/?next=/basket/items/')
        self.assertEqual(response.status_code, 302)

        self.client.login(username='simple_user', password='password')

        Basket.objects.create(customer=self.user)
        response = self.client.get('/basket/items/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['basket'])

    def test_user_logout(self):

        self.client.login(username='simple_user', password='password')

        response = self.client.get('/')
        self.assertEqual(response.context['user'], self.user)
        self.assertFalse(response.context['user'].is_anonymous)

        response = self.client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_anonymous)

    def test_user_register(self):

        response = self.client.get('/accounts/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_anonymous)

        new_user_data = {
            'username': 'Detective',
            'first_name': 'Saga',
            'last_name': 'Nuren',
            'password1': '12435678Ll!',
            'password2': '12435678Ll!',
            'email': 'saga@gmail.com',
            'age': '35'
        }

        response = self.client.post('/accounts/register/', data=new_user_data)
        self.assertEqual(response.status_code, 302)

        new_user = User.objects.get(username=new_user_data['username'])
        activation_url = '{}/accounts/verify/{}/{}/'.format(
            settings.DOMAIN_NAME, new_user_data['username'], new_user.activation_key)

        response = self.client.get(activation_url)
        self.assertEqual(response.status_code, 200)

        self.client.login(
            username=new_user_data['username'], password=new_user_data['password1'])

        response = self.client.get('/products/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'], new_user_data['username'])

    def test_wrong_register(self):

        new_user_data = {
            'username': 'Detective',
            'first_name': 'Saga',
            'last_name': 'Nuren',
            'password1': '12435678Ll!',
            'password2': '12435678Ll!',
            'email': 'saga@gmail.com',
            'age': '17'
        }

        response = self.client.post('/accounts/register/', data=new_user_data)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'age', 'You are very young!')

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp',
                     'accounts', 'basketapp', 'ordersapp')
