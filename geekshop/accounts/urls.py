from django.urls import path
from accounts import views
from pathlib import PurePath
import os

app_name = PurePath(os.path.dirname(__file__)).parts[-1]

urlpatterns = [
    path('register/', views.UserCreateView.as_view(), name='registration'),
    path('register/confirm/',
         views.RegistrationConfirm.as_view(), name='confirm'),
    path('verify/<str:username>/<str:key>/',
         views.VerifyEmail.as_view(), name='verify'),
    path('login/', views.UserLogin.as_view(), name='login'),
    path('logout/', views.UserLogout.as_view(), name='logout'),
    path('update/', views.UserUpdateView.as_view(), name='update'),
]
