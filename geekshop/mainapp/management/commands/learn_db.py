from django.core.management.base import BaseCommand
from mainapp.models import Product
from django.db.models import Q
from django.db import connection


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        test_products = Product.objects.filter(
            Q(category__title='Classic') | Q(category__title='Running')).select_related('category')

        print('\nQueryset:\n\n', test_products)
        print('\nNumber of items in queryset: {}'.format(len(test_products)))
        print('\nNumber of queries: {}'.format(len(connection.queries)))
        print("\nQueries:")
        for query in connection.queries:
            print('\n', query)
