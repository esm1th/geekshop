from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.conf import settings
from django.core.cache import cache

from .models import Product
from .forms import AddBasketItemForm
from basketapp.views import BasketUpdate

# Create your views here.


class MainView(ListView):
    """Class process main page"""

    model = Product
    ordering = ['?']
    template_name = 'mainapp/index.html'

    def get_queryset(self):
        queryset = super().get_queryset().prefetch_related(
            'pictures').select_related('creator')
        return queryset


class ProductList(ListView):
    """Class process catalogue page"""

    model = Product
    paginated_by = 8
    columns = 2
    orphans = 0

    def get(self, request, **kwargs):
        response = super().get(request, **kwargs)

        if self.request.is_ajax():
            result = render_to_string(
                'mainapp/includes/inc_product_list.html',
                context=self.get_context_data(),
                request=self.request
            )
            return JsonResponse({'result': result})
        else:
            return response

    def get_queryset(self):
        queryset = super().get_queryset().select_related(
            'creator', 'category').prefetch_related('pictures')
        if 'pk' in self.kwargs:
            queryset = queryset.filter(category=self.kwargs.get('pk'))
        return queryset

    def get_paginated_rows(self, dataset):
        paginator = self.paginator_class(dataset, self.columns, self.orphans)
        for page_num in paginator.page_range:
            yield paginator.page(page_num).object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(
            categories=self.get_categories(), **kwargs)
        context.update(
            {'obj_pages':
                [row for row in self.get_paginated_rows(self.object_list)]})
        return context

    # this is for cache testing
    # if you want check how it works add cache_page(3600) to this view
    # in urls.py
    def get_categories(self):
        if settings.LOW_CACHE:
            key = 'categories'
            categories = cache.get(key)
            if categories is None:
                categories = self.model.category.get_queryset()
                cache.set(key, categories)
            return categories
        else:
            return self.model.category.get_queryset()


class ProductDetail(LoginRequiredMixin, FormView, DetailView):
    """Class process page with detail info about product"""

    model = Product
    form_class = AddBasketItemForm

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        if request.is_ajax():
            result = render_to_string(
                'mainapp/includes/inc_product_detail_ajax.html',
                context=self.get_context_data(),
                request=request
            )
            return JsonResponse({'result': result})
        else:
            return response

    def get_queryset(self):
        return super().get_queryset().select_related(
            'category', 'creator').prefetch_related('pictures')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        similar = self.get_queryset().exclude(
            id=self.object.id).filter(
                category=self.object.category).order_by('?')
        context.update({'similar': similar})
        return context

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs.update({'product': self.object})
        return kwargs

    def post(self, request, **kwargs):
        self.object = self.get_object()
        response = super().post(request, **kwargs)

        if request.is_ajax():
            self.object.refresh_from_db()
            card = request.user.basket.last()
            summary = card.get_summary

            data = {
                'price': summary['total_price'],
                'card_quantity': summary['total_items_quantity'],
                'product_quantity': self.object.quantity
            }

            if self.object.quantity == 0:
                html = render_to_string(
                    'mainapp/includes/inc_product_not_exists.html',
                    context=self.get_context_data(),
                    request=request
                )
                data.update({'html': html})

            return JsonResponse(data)

        return response

    def form_valid(self, form):
        self.kwargs.update({'quantity': form.cleaned_data.get('quantity')})
        return BasketUpdate.as_view()(self.request, *self.args, **self.kwargs)
