window.onload = function() {
    $('.categories').on('click', 'a', function(event) {
        var link = event.target.href;

        $.ajax({
            url: link,
            success: function(data) {
                $('.category_list').html(data.result);
            },
        });

        $('.categories a').removeClass();
        $(event.target).addClass('checkmark');

        history.replaceState(null, null, link);
        event.preventDefault();
    });


    $(document).on('click', '.sim-pic > a', function(event) {
        var link = event.target.closest('a').href;

        $.ajax({
            url: link,
            success: function(data) {
                $('.product').html(data.result);
            },
        });

        history.replaceState(null, null, link);
        event.preventDefault();
    });


    $(document).on('click', '.product-data button', function() {
        var closest_div = event.target.closest('div');
        var form = $(closest_div).find('form');
        var method = form.attr('method');
        var token = form.find('input[name="csrfmiddlewaretoken"]').attr('value');
        var quantity = $(form).find('input[type="number"]').val();

        $.ajax ({
          method: method,
          headers: {'X-CSRFToken': token},
          url: window.location.href,
          data: {'quantity': quantity},
          success: function(data) {
            $('.price span').html(parseInt(data.price));
            $('.card-pic p').html(data.card_quantity);
            $('.prod_quantity span').html(data.product_quantity);
            if (data.html) {
              $(form).remove();
              $('.product-data').append(data.html);
            };
          },
        });

      event.preventDefault();
    });
};
