from django.test import TestCase
from mainapp.models import Category, Product


class ProductsTest(TestCase):
    """Class for testing products"""

    def setUp(self):
        self.category = Category.objects.create(title='Football')
        self.product_1 = Product.objects.create(
            title='Nike football',
            category=self.category,
            price=7500,
            quantity=7
        )
        self.product_2 = Product.objects.create(
            title='Adidas football',
            category=self.category,
            price=5000,
            quantity=5
        )

    def test_get_product(self):
        product_1 = Product.objects.get(title='Nike football')
        product_2 = Product.objects.get(title='Adidas football')
        self.assertEqual(product_1, self.product_1)
        self.assertEqual(product_2, self.product_2)

    def test_product_discount(self):
        self.category.discount = 10
        self.category.save()
        product_1 = Product.objects.get(title='Nike football')
        self.assertEqual(product_1.price, self.product_1.price)
        self.assertNotEqual(product_1.discount_price, product_1.price)

    def test_products_category(self):
        product_1 = Product.objects.get(title='Nike football')
        product_2 = Product.objects.get(title='Adidas football')
        queryset = self.category.product_set.all()
        self.assertEqual(list(queryset), [product_1, product_2])
