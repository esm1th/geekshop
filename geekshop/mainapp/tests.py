from django.test import TestCase
from django.core.management import call_command
from django.urls import reverse
from mainapp.models import Product, Category

# Create your tests here.


def user_login(object, url, username, password):
    """Return response after post login data"""
    credentials = {'username': username, 'password': password}
    return object.client.post(url, data=credentials)


class TestMainappSmoke(TestCase):
    """Smoke test for Mainapp"""

    def setUp(self):
        call_command('flush', '--noinput')
        call_command('loaddata', 'test_db.json')

    def test_mainapp_urls(self):
        error = "Please enter a correct username and password. Note that both" \
                " fields may be case-sensitive."
        url = reverse('accounts:login')

        response = user_login(self, url, 'test', 'wrong password')
        self.assertFormError(response, 'form', None, error)
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('mainapp:detail', kwargs={'pk': 3}))
        self.assertEqual(response.status_code, 302)

        response = user_login(self, url, 'test', 'test')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response._headers['location'][1], '/')

        response = self.client.get('')
        self.assertEqual(response._request.user.username, 'test')
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse('mainapp:products'))
        self.assertEqual(response.status_code, 200)

        for pk in Category.objects.values_list('id', flat=True):
            response = self.client.get(
                reverse('mainapp:products_category', kwargs={'pk': pk})
            )
            self.assertEqual(response.status_code, 200)

        for pk in Product.objects.values_list('id', flat=True):
            response = self.client.get('/product/{}/'.format(pk))

    def tearDown(self):
        call_command('sqlsequencereset', 'mainapp')
