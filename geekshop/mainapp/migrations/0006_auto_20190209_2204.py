# Generated by Django 2.1.4 on 2019-02-09 19:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0005_product_dicount'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='dicount',
            new_name='discount',
        ),
    ]
