from django.utils.translation import ugettext_lazy as _
from django import forms
from accounts.forms import ChangeWidgetMixin


class AddBasketItemForm(ChangeWidgetMixin, forms.Form):
    quantity = forms.IntegerField(initial=1, min_value=1, max_value=20)

    def __init__(self, product, *args, **kwargs):
        self.product = product
        return super().__init__(**kwargs)

    def clean_quantity(self):
        clean_quantity = self.cleaned_data.get('quantity')
        if not clean_quantity <= self.product.quantity:
            raise forms.ValidationError(
                _('Entered quantity greater than stock quantity!'))
        return clean_quantity
