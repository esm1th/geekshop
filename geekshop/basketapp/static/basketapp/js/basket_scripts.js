window.onload = function () {
    $('.basket_item').on('change', 'input[type="number"]', function (event) {

        var init_object = event.target;
        var closest_div = init_object.closest('div');

        $.ajax ({

            url: init_object.getAttribute('url'),
            data: {'quantity': init_object.value},
            success: function(data) {
                if (data.deleted) {
                    $(closest_div).remove();
                    $('.card-pic').children('p').html(data.total_items_quantity);
                } else {
                    $(closest_div).children('p').children('span').html(data.price);
                };
                $('.price').children('span').html(data.total_price);
            },

        });

        event.preventDefault();
    });



    $('.basket_item').on('click', 'button', function (event) {

        this.type = 'button';

        var div = this.closest('.basket_item');
        var link = div.querySelector('form').getAttribute('action');
        var method = div.querySelector('form').getAttribute('method');
        var token = div.querySelector('form input[name="csrfmiddlewaretoken"]').value;

        $.ajax ({
            
            type: method,
            headers: { 'X-CSRFToken': token },
            url: link,
            success: function(data) {
                if (data.html) {
                    $('.user_card').html(data.html);
                } else {
                   div.remove(); 
                };
                $('.price span').text(data.total_price);
                $('.card-pic p').text(data.items_quantity);
            },
            
        });
    }); 
}