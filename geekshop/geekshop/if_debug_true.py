import os
from debug_toolbar.settings import PANELS_DEFAULTS


# settings for debug_toolbar app
INTERNAL_IPS = [
    '127.0.0.1'
]

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_COLLAPSED": True
}

DEBUG_TOOLBAR_PANELS = PANELS_DEFAULTS + [
    'debug_toolbar.panels.profiling.ProfilingPanel',
    'template_profiler_panel.panels.template.TemplateProfilerPanel'
]

# database settings
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# email settings
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DOMAIN_NAME = 'http://localhost:8000'
EMAIL_HOST = 'localhost'
EMAIL_PORT = '25'
EMAIL_HOST_USER = 'django@geekshop.local'
EMAIL_HOST_PASSWORD = 'geekshop'
EMAIL_USE_SSL = True
