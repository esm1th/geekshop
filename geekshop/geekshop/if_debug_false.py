import os

#  database settings
DATABASES = {
    'default': {
        'NAME': 'geekshop',
        'ENGINE': 'django.db.backends.postgresql',
        'USER': 'django',
        'PASSWORD': 'geekbrains',
        'HOST': 'localhost'
    }
}

# email settings - yandex
DOMAIN_NAME = 'geekshop.website'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'admin@geekshop.website'
EMAIL_USE_SSL = True

SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

SETTINGS_DIR = os.path.dirname(os.path.abspath(__file__))

with open(os.path.join(SETTINGS_DIR, 'mail.txt'), 'r') as file:
    EMAIL_HOST_PASSWORD = file.read()
