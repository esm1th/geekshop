from django.urls import path
from pathlib import PurePath
import ordersapp.views as views
import os

app_name = PurePath(os.path.dirname(__file__)).parts[-1]

urlpatterns = [
    path('list/', views.OrderList.as_view(), name='list'),
    path('create/', views.OrderItemsCreate.as_view(), name='create'),
    path('update/<int:pk>/',
         views.OrderStatusUpdate.as_view(), name='status_update'),
    path('update/<int:pk>/items/',
         views.OrderItemsUpdate.as_view(), name='items_update'),
    path('item/get/price/<int:pk>/',
         views.FetchOrderItemPrice.as_view(), name='get_price'),
    path('detail/<int:pk>/', views.OrderDetail.as_view(), name='detail'),
    path('delete/order/<int:pk>/', views.OrderDelete.as_view(), name='delete'),
    path('delete/item/<int:pk>/',
         views.OrderItemDelete.as_view(), name='item_delete'),
]
